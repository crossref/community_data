CREATE SCHEMA IF NOT EXISTS community;
CREATE USER 'community_service'@'%' IDENTIFIED BY 'comm_S3rv1ce';
GRANT ALL ON community.* TO 'community_service'@'%';