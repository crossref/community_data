FROM adoptopenjdk/openjdk11:alpine-jre
COPY build/libs/*.jar /app.jar
RUN apk add --update curl && rm -rf /var/cache/apk/*
ENTRYPOINT ["sh", "-c", "SENTRY_ENVIRONMENT=$ENV java -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]
HEALTHCHECK --start-period=20s \
  CMD curl http://localhost:80/actuator/health | egrep -o '^{"status":"UP"'
