image: registry.gitlab.com/crossref/kotlin-dev-docker

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  MAINT_BRANCH_PATTERN: /^(([0-9]+)\.)?([0-9]+)\.x/
  PREREL_BRANCH_PATTERN: /^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$/

stages:
  - get-version
  - test
  - analysis
  - deploy-stg
  - deploy-prd

# Build and test
tests:
  image: docker/compose
  stage: test
  services:
    - docker:dind

  before_script:
    - docker info

  script:
    - docker-compose -f docker-compose-integration-test.yml up --exit-code-from community-data-test

  cache:
    key: gradle-cache-key
    paths:
      - .gradle
      - build
    policy: push
  artifacts:
    paths:
      - target/*.jar
      - build/libs/*.jar
      - build/reports/jacoco/test/*.xml
    reports:
      junit:
        - build/test-results/test/TEST-*.xml

# Code analysis
sonarcloud-check:
  stage: analysis
  dependencies:
    - tests
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .gradle
      - .sonar
      - build
  script:
    - gradle --build-cache sonarqube --info
    - ktlint
  except:
    - pipelines

# Build image and deploy to staging
deploy-staging:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: deploy-stg
  variables:
    AWS_DEFAULT_PROFILE: staging
  tags:
    - aws
    - crossref-portal
  services:
    - docker:dind
  before_script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - echo "APP_VERSION is ${APP_VERSION}"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - npx semantic-release
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME" -t "$CI_REGISTRY_IMAGE:$APP_VERSION" -t "$CI_REGISTRY_IMAGE:latest" .
    - docker push "$CI_REGISTRY_IMAGE:$APP_VERSION"
    - docker push "$CI_REGISTRY_IMAGE:latest"
    - upload_docker_image.sh $AWS_PRIMARY_REGION $AWS_ACCOUNT_ID_STAGING community-data-staging $CI_REGISTRY_IMAGE $APP_VERSION
    - update_ecs_service.sh $AWS_PRIMARY_REGION community-data-staging $APP_VERSION
  only:
    - alpha
    - main
  artifacts:
    paths:
      - CHANGELOG.md
  except:
    - pipelines

## Optionally deploy image to sandbox
deploy-sandbox:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: deploy-prd
  variables:
    AWS_DEFAULT_PROFILE: sandbox
  tags:
    - aws
    - crossref-portal
  services:
    - docker:dind
  when: manual
  before_script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - echo "APP_VERSION is ${APP_VERSION}"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker pull registry.gitlab.com/crossref/community_data:$APP_VERSION
  script:
    - upload_docker_image.sh $AWS_PRIMARY_REGION $AWS_ACCOUNT_ID_SANDBOX community-data-sandbox $CI_REGISTRY_IMAGE $APP_VERSION
    - update_ecs_service.sh $AWS_PRIMARY_REGION community-data-sandbox $APP_VERSION
  only:
    - main
  except:
    - pipelines

## Optionally deploy image to production
deploy-production:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: deploy-prd
  variables:
    AWS_DEFAULT_PROFILE: production
  tags:
    - aws
    - crossref-portal
  when: manual
  services:
    - docker:dind
  before_script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - echo "APP_VERSION is ${APP_VERSION}"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker pull registry.gitlab.com/crossref/community_data:$APP_VERSION
  script:
    - upload_docker_image.sh $AWS_PRIMARY_REGION $AWS_ACCOUNT_ID_PRODUCTION community-data-production $CI_REGISTRY_IMAGE $APP_VERSION
    - update_ecs_service.sh $AWS_PRIMARY_REGION community-data-production $APP_VERSION
  only:
    - main
  except:
    - pipelines

# Get current semantic version for release branches
get-semantic-version:
  image: node:14
  stage: get-version
  only:
    refs:
      - main
      - alpha
      - $MAINT_BRANCH_PATTERN
      - $PREREL_BRANCH_PATTERN
  script:
    - echo "Using semantic version"
    - npm install @semantic-release/gitlab @semantic-release/exec @semantic-release/changelog
    - npx semantic-release --dry-run
  artifacts:
    paths:
      - VERSION.txt
  except:
    - pipelines

# Get generic version for non-release branches
get-generic-version:
  stage: get-version
  except:
    refs:
      - main
      - alpha
      - $MAINT_BRANCH_PATTERN
      - $PREREL_BRANCH_PATTERN
  script:
    - echo "Using generic version"
    - echo build-$CI_PIPELINE_ID > VERSION.txt
  artifacts:
    paths:
      - VERSION.txt
  except:
    - pipelines

cache:
  key: gradle-cache-key
  paths:
    - .gradle
    - build
    - .sonar
  policy: pull
