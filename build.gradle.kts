import org.gradle.language.jvm.tasks.ProcessResources
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.4.1"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    id("org.sonarqube") version "3.0"
    jacoco
    kotlin("jvm") version "1.4.21"
    kotlin("plugin.spring") version "1.4.21"
    id("org.jetbrains.dokka") version "1.4.20"
}

group = "org.crossref"
version = System.getenv("APP_VERSION")
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
    jcenter()
}

(tasks.getByName("processResources") as ProcessResources).apply {
    filesMatching("application.properties") {
        expand(project.properties)
    }
}

configurations.all {
    resolutionStrategy {
        activateDependencyLocking()
        failOnVersionConflict()
        eachDependency {
            when (requested.module.toString()) {
                "org.apache.logging.log4j:log4j-api" -> useVersion("2.16.0")
                "org.apache.logging.log4j:log4j-to-slf4j" -> useVersion("2.16.0")
                "io.swagger.core.v3:swagger-annotations" -> useVersion("2.1.11")
                "io.github.classgraph:classgraph" -> useVersion("4.8.90")
            }
        }
    }
}

dependencies {

    // Databases
    implementation("mysql:mysql-connector-java:8.0.19")
    implementation("org.jdbi:jdbi3-core:3.12.2")
    implementation("org.liquibase:liquibase-core:3.8.7")

    // mocking
    implementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    implementation("com.github.KennethWussmann:mock-fuel:1.3.0")

    // logging
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("net.logstash.logback:logstash-logback-encoder:6.6")
    implementation("com.fasterxml.jackson.module:jackson-module-jaxb-annotations:2.12.1")
    implementation("org.codehaus.janino:janino:3.1.2")

    // basic kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Http clint
    implementation("com.github.kittinunf.fuel:fuel:2.3.1")
    implementation("com.github.kittinunf.fuel:fuel-jackson:2.3.1")

    // spring
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-jdbc")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.security.oauth:spring-security-oauth2:2.5.0.RELEASE")
    implementation("org.springframework.security:spring-security-jwt:1.1.1.RELEASE")
    implementation("org.springframework.security:spring-security-test")
    implementation("javax.xml.bind:jaxb-api:2.3.1")
    implementation("javax.activation:activation:1.1")
    implementation("org.glassfish.jaxb:jaxb-runtime:2.3.3")

    // testing
    implementation("org.junit.platform:junit-platform-runner:1.7.0")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
        exclude(module = "mockito-core")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

    // springdoc
    implementation("org.springdoc:springdoc-openapi-data-rest:1.5.2")
    implementation("org.springdoc:springdoc-openapi-ui:1.5.2")
    implementation("org.springdoc:springdoc-openapi-kotlin:1.5.2")
    implementation("io.springfox:springfox-swagger2:3.0.0")

    // Sentry
    implementation("io.sentry:sentry-spring-boot-starter:4.3.0")

    // dokka
    dokkaHtmlPlugin("org.jetbrains.dokka:kotlin-as-java-plugin:1.4.20")
}

sonarqube {
    properties {
        property("sonar.projectKey", "crossref_community_data")
        property("sonar.organization", "crossref")
    }
}

tasks.jacocoTestReport {
    reports {
        html.isEnabled = false
        xml.isEnabled = true
        csv.isEnabled = false
    }

    finalizedBy("jacocoTestCoverageVerification")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
