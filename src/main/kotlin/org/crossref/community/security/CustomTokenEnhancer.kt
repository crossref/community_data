package org.crossref.community.security

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.TokenEnhancer
import java.util.Calendar

/**
 * Customize generated access token.
 */
class CustomTokenEnhancer : TokenEnhancer {
    override fun enhance(accessToken: OAuth2AccessToken, authentication: OAuth2Authentication): OAuth2AccessToken {
        val defToken = accessToken as DefaultOAuth2AccessToken

        // Set expiration
        val expiration = Calendar.getInstance()
        expiration.add(Calendar.DAY_OF_MONTH, 1)
        defToken.expiration = expiration.time
        return accessToken
    }
}
