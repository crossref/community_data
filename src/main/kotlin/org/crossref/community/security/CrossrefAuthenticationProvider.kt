package org.crossref.community.security

import org.crossref.community.services.UserDataService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority

class CrossrefAuthenticationProvider : AuthenticationProvider {

    @Autowired
    lateinit var userDataService: UserDataService

    override fun authenticate(authentication: Authentication): Authentication {
        val username = authentication.getName() ?: ""
        val password = authentication.getCredentials()?.toString() ?: ""

        val resp = userDataService.authenticate(username, password)

        if (resp.hasAuthFail()) {
            // final static strings
            throw BadCredentialsException("Wrong credentials. Incorrect username or password.")
        } else if (resp.isServiceDown()) {
            throw AuthenticationServiceException("There is a problem with the authentication service. Please try again later or contact support.")
        }

        if (username == "root") {
            return UsernamePasswordAuthenticationToken(username, password, listOf(SimpleGrantedAuthority("ROLE_ADMIN")))
        }
        return UsernamePasswordAuthenticationToken(username, password, listOf(SimpleGrantedAuthority("ROLE_USER")))
    }

    public override fun supports(authClass: java.lang.Class<*>?): Boolean {
        return (authClass == UsernamePasswordAuthenticationToken::class.java)
    }
}
