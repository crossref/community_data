package org.crossref.community.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Repository

private class MemberDetails(uname: String) : UserDetails {
    private val username: String = uname
    private var password: String? = null
    private var accountNonExpired = true
    private var accountNonLocked = true
    private var credentialsNonExpired = true
    private var enabled = true
    private val authorities: MutableSet<GrantedAuthority> = HashSet()

    fun addAuthority(role: String) {
        authorities.add(SimpleGrantedAuthority(role))
    }

    override fun getAuthorities() = authorities
    override fun getPassword() = password
    override fun getUsername() = username
    override fun isAccountNonExpired() = accountNonExpired
    override fun isAccountNonLocked() = accountNonLocked
    override fun isCredentialsNonExpired() = credentialsNonExpired
    override fun isEnabled() = enabled
}

@Repository
class CustomUserDetailsService : UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails {
        // Until we get a real custom user details service in place
        val details = MemberDetails(username)
        return details
    }
}
