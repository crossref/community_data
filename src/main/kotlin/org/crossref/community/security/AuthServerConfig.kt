package org.crossref.community.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import java.util.Arrays
import javax.sql.DataSource

@Configuration
@EnableAuthorizationServer
class AuthServerConfig : AuthorizationServerConfigurerAdapter() {

    @Value("\${jwt.signing.key}")
    private val jwtSigningKey: String? = null

    @Value("\${cs.client.secret}")
    private val clientSecret: String? = null

    @Autowired
    lateinit var customUserDetailsService: CustomUserDetailsService

    @Autowired
    @Qualifier("serviceDs")
    val authDs: DataSource? = null

    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Throws(Exception::class)
    override fun configure(oauthServer: AuthorizationServerSecurityConfigurer) {
        oauthServer
            .checkTokenAccess("isAuthenticated()")
    }

    @Bean
    fun tokenEnhancer(): CustomTokenEnhancer? {
        return CustomTokenEnhancer()
    }

    @Bean
    fun jwtTokenStore(): TokenStore? {
        return JwtTokenStore(jwtTokenConverter())
    }

    @Bean
    fun jwtTokenConverter(): JwtAccessTokenConverter? {
        val converter = JwtAccessTokenConverter()
        converter.setSigningKey(jwtSigningKey)
        return converter
    }

    @Throws(Exception::class)
    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.inMemory()
            .withClient("cs")
            .authorizedGrantTypes("password", "refresh_token", "authorization_code", "client_credentials")
            .scopes("read", "write")
            .secret(BCryptPasswordEncoder().encode(clientSecret))
            .accessTokenValiditySeconds(300)
    }

    @Throws(Exception::class)
    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        val tokenEnhancerChain = TokenEnhancerChain()
        tokenEnhancerChain.setTokenEnhancers(
            Arrays.asList(tokenEnhancer(), jwtTokenConverter())
        )

        endpoints
            .tokenStore(jwtTokenStore())
            .tokenEnhancer(tokenEnhancerChain)
            .authenticationManager(authenticationManager)
            .userDetailsService(customUserDetailsService)
    }
}
