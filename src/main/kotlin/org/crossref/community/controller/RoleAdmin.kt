package org.crossref.community.controller

import org.crossref.community.services.UserDataService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
class RoleAdmin {
    @Autowired
    lateinit var userDataService: UserDataService

    @RequestMapping(value = ["/admin/addrole/{name}"], method = [(RequestMethod.GET)])
    fun addrole(@PathVariable name: String): ResponseEntity<Any> {
        return ResponseEntity.ok(mapOf("success" to userDataService.createRole(name)))
    }

    @RequestMapping(value = ["/admin/delrole/{name}"], method = [(RequestMethod.GET)])
    fun delrole(@PathVariable name: String): ResponseEntity<Any> {
        return ResponseEntity.ok(mapOf("success" to userDataService.deleteRole(name)))
    }
}
