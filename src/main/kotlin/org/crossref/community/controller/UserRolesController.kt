package org.crossref.community.controller

import org.crossref.community.services.UserDataService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
class UserRolesController {
    @Autowired
    lateinit var userDataService: UserDataService

    data class UserRolesResponse(val roles: List<String>)

    @RequestMapping(value = ["/private/roles"], method = [(RequestMethod.GET)])
    fun getUserRoles(): ResponseEntity<Any> {
        val principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal()

        val username = if (principal is UserDetails) principal.getUsername() else principal.toString()
        return ResponseEntity.ok(
            UserRolesResponse(
                userDataService.getRoles(username).map { it.name }
            )
        )
    }
}
