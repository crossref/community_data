package org.crossref.community.model

data class UserRole(val name: String)
