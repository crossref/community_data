package org.crossref.community.model

class AuthenticationResponse(val status: AuthStatus, val message: String) {
    enum class AuthStatus {
        SUCCESS,
        WRONG_CREDENTIALS,
        SERVICE_ERROR
    }

    fun isSuccess() = status == AuthStatus.SUCCESS
    fun isServiceDown() = status == AuthStatus.SERVICE_ERROR
    fun hasAuthFail() = status == AuthStatus.WRONG_CREDENTIALS
}
