package org.crossref.community.services

import org.crossref.community.model.AuthenticationResponse
import org.crossref.community.model.UserRole

interface UserDataService {
    fun authenticate(userName: String, password: String): AuthenticationResponse
    fun getRoles(userName: String): List<UserRole>
    fun deleteRole(name: String): Boolean
    fun createRole(name: String): Boolean
    fun isAvailable(): Boolean
}
