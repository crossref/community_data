package org.crossref.community.services.impl

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.jackson.objectBody
import com.github.kittinunf.fuel.jackson.responseObject
import com.github.kittinunf.result.Result
import org.crossref.community.model.AuthenticationResponse
import org.crossref.community.model.UserRole
import org.crossref.community.services.UserDataService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import java.net.URL

class AuthenticatorService(val baseUrl: String, val basicUser: String, val basicPwd: String) : UserDataService {

    val AUTHENTICATE_PATH = "/api/v1/authenticate"
    val USERINFO_PATH = "/api/v1/user-info"
    val HEARTBEAT_PATH = "/heartbeat"
    val ADDROLE_PATH = "/api/v1/add-role"
    val DELROLE_PATH = "/api/v1/del-role"
    val HTTP_BASIC_FAIL = "Basic HTTP authentication failed"

    val mapper = ObjectMapper()
    data class Credentials(val username: String, val password: String)

    companion object {
        val logger = LoggerFactory.getLogger(AuthenticatorService::class.java)
    }

    data class AuthOkResponse(
        @JsonProperty("status") val status: String,
        @JsonProperty("username") val username: String
    )

    data class GenericResponse(
        @JsonProperty("status") val status: String,
        @JsonProperty("message") val message: String
    )

    data class UserInfoResponse(
        @JsonProperty("status") val status: String,
        @JsonProperty("username") val username: String?,
        @JsonProperty("legacy_roles") val roles: List<String>?,
        @JsonProperty("message") val message: String?
    )

    data class HeartBeat(
        @JsonProperty("status") val status: String,
    )

    override fun authenticate(userName: String, password: String): AuthenticationResponse {
        return doAuth(userName, password)
    }

    fun doAuth(userName: String, password: String, attempts: Int = 3): AuthenticationResponse {
        val baseUrlHost = URL(baseUrl)
        val authUrl = URL(baseUrlHost.protocol, baseUrlHost.host, baseUrlHost.port, AUTHENTICATE_PATH).toString()

        val (_, response, result) = Fuel.post(authUrl)
            .objectBody(Credentials(userName, password))
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
            .authentication()
            .basic(basicUser, basicPwd)
            .responseObject<AuthOkResponse>()

        when (result) {
            is Result.Success -> return AuthenticationResponse(AuthenticationResponse.AuthStatus.SUCCESS, "Authenticated")
            is Result.Failure -> {
                if (response.statusCode >= 400) {
                    val str = response.body().asString(MediaType.APPLICATION_JSON.toString())
                    try {
                        val res = mapper.readValue(str, GenericResponse::class.java)
                        val status = if (res.message == HTTP_BASIC_FAIL) {
                            AuthenticationResponse.AuthStatus.SERVICE_ERROR
                        } else {
                            logger.warn("Wrong credentials supplied", response.responseMessage)
                            AuthenticationResponse.AuthStatus.WRONG_CREDENTIALS
                        }
                        return AuthenticationResponse(status, res.message)
                    } catch (_: JsonParseException) {
                        if (attempts > 0) {
                            return doAuth(userName, password, attempts - 1)
                        } else {
                            return AuthenticationResponse(AuthenticationResponse.AuthStatus.SERVICE_ERROR, "Authenticator service error: $str")
                        }
                    }
                }
                logger.warn("Authenticator service not accessible", Exception(response.responseMessage))
                return AuthenticationResponse(AuthenticationResponse.AuthStatus.SERVICE_ERROR, "Internal Error")
            }
        }
    }

    override fun getRoles(userName: String): List<UserRole> {

        val baseUrlHost = URL(baseUrl)
        val authUrl = URL(baseUrlHost.protocol, baseUrlHost.host, baseUrlHost.port, USERINFO_PATH).toString()

        val (_, _, result) = Fuel.post(authUrl)
            .objectBody(Credentials(userName, ""))
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
            .authentication()
            .basic(basicUser, basicPwd)
            .responseObject<UserInfoResponse>()

        when (result) {
            is Result.Success -> return result.get().roles?.map { UserRole(it) } ?: listOf()
        }
        return listOf()
    }

    override fun deleteRole(name: String): Boolean {
        val baseUrlHost = URL(baseUrl)
        val authUrl = URL(baseUrlHost.protocol, baseUrlHost.host, baseUrlHost.port, "$DELROLE_PATH/$name").toString()

        val (_, _, result) = Fuel.get(authUrl)
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
            .authentication()
            .basic(basicUser, basicPwd)
            .responseObject<GenericResponse>()

        when (result) {
            is Result.Success -> return result.get().status == "success"
        }
        return false
    }

    override fun createRole(name: String): Boolean {
        val baseUrlHost = URL(baseUrl)
        val authUrl = URL(baseUrlHost.protocol, baseUrlHost.host, baseUrlHost.port, "$ADDROLE_PATH/$name").toString()

        val (_, _, result) = Fuel.get(authUrl)
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
            .authentication()
            .basic(basicUser, basicPwd)
            .responseObject<GenericResponse>()

        when (result) {
            is Result.Success -> return result.get().status == "success"
        }
        return false
    }

    override fun isAvailable(): Boolean {
        val baseUrlHost = URL(baseUrl)
        val aliveUrl = URL(baseUrlHost.protocol, baseUrlHost.host, baseUrlHost.port, HEARTBEAT_PATH).toString()

        val (_, _, result) = Fuel.get(aliveUrl).responseObject<HeartBeat>()
        when (result) {
            is Result.Success -> return result.value.status == "ok"
        }
        return false
    }
}
