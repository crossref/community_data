package org.crossref.community

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CommunityDataApp

fun main(args: Array<String>) {
    runApplication<CommunityDataApp>(*args)
}
