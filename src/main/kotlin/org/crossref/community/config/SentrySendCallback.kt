package org.crossref.community.config

import io.sentry.SentryEvent
import io.sentry.SentryOptions
import org.springframework.stereotype.Component

@Component
class SentrySendCallback : SentryOptions.BeforeSendCallback {

    val IGNORED_EXCEPTIONS = setOf("InvalidGrantException")

    override fun execute(event: SentryEvent, hint: Any?): SentryEvent? {
        if (IGNORED_EXCEPTIONS.intersect(event.getExceptions().map { it.type }).isNotEmpty()) {
            return null
        }

        return event
    }
}
