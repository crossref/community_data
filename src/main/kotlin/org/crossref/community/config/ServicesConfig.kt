package org.crossref.community.config

import org.crossref.community.actuators.HealthCheck
import org.crossref.community.services.UserDataService
import org.crossref.community.services.impl.AuthenticatorService
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan("org.crossref")
class ServicesConfig {
    @Value("\${auth.svc.baseurl}")
    private val authClientBaseUrl: String = ""

    @Value("\${auth.svc.user}")
    private val authSvcUser: String = ""

    @Value("\${auth.svc.password}")
    private val authSvcPassword: String = ""

    @Bean
    fun authenticationService(): UserDataService {
        return AuthenticatorService(authClientBaseUrl, authSvcUser, authSvcPassword)
    }

    @Bean
    fun authServiceHealthIndicator(authService: UserDataService): HealthIndicator {
        return HealthCheck(authService)
    }
}
