package org.crossref.community.config

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import springfox.documentation.builders.PathSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@org.springframework.context.annotation.Configuration
@ComponentScan("org.crossref")
@EnableAutoConfiguration(exclude = [DataSourceAutoConfiguration::class])
@EnableSwagger2
class AppConfig {
    @org.springframework.beans.factory.annotation.Value("\${cs.client.secret}")
    private val csClientSecret: kotlin.String? = null

    @org.springframework.context.annotation.Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
            .select()
            .paths(PathSelectors.ant("/api/**"))
            .build()
    }
}
