package org.crossref.community.actuators

import org.crossref.community.services.UserDataService
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator

class HealthCheck(private val userDataService: UserDataService) : HealthIndicator {
    companion object {
        const val AUTHENTICATOR_STATUS = "Available"
    }

    @Value("\${info.app.version}")
    private val version: String = ""

    override fun health(): Health {
        val ok = userDataService.isAvailable()
        val builder = if (ok) Health.up() else Health.down()

        return builder.withDetail(AUTHENTICATOR_STATUS, ok)
            .withDetail("VER", version)
            .withDetail("ENV", System.getenv("ENV") ?: "unset")
            .build()
    }
}
