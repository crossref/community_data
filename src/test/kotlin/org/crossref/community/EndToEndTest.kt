package org.crossref.community

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import org.crossref.community.config.AppConfig
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.security.web.FilterChainProxy
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@Extensions(ExtendWith(SpringExtension::class))
@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(AppConfig::class))
@WebAppConfiguration
class EndToEndTest {
    @Autowired
    lateinit var webApplicationContext: WebApplicationContext

    @Autowired
    lateinit var springSecurityFilterChain: FilterChainProxy

    lateinit var mockMvc: MockMvc
    val mapper = ObjectMapper()

    data class AuthOkResponse(
        @JsonProperty("access_token") val access_token: String,
        @JsonProperty("refresh_token") val refresh_token: String,
        @JsonProperty("token_type") val token_type: String,
        @JsonProperty("expires_in") val expires_in: String,
        @JsonProperty("scope") val scope: String,
        @JsonProperty("jti") val jti: String
    )

    @BeforeEach
    @Throws(Exception::class)
    fun setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
            .addFilter<DefaultMockMvcBuilder>(springSecurityFilterChain)
            .build()
    }

    fun DoAuth(user: String, pwd: String) = mockMvc
        .perform(
            MockMvcRequestBuilders.post("/oauth/token")
                .param("username", user).param("password", pwd).param("grant_type", "password")
                .with(SecurityMockMvcRequestPostProcessors.httpBasic("cs", "a55557848a884359a980dce828af33b8"))
        )

    fun GetCredentials(user: String, pwd: String) = mapper.readValue(
        DoAuth(user, pwd).andReturn().response.contentAsString,
        AuthOkResponse::class.java
    )

    fun DoGet(path: String, creds: AuthOkResponse) = mockMvc
        .perform(
            MockMvcRequestBuilders.get(path)
                .header("Authorization", "Bearer ${creds.access_token}")
                .header("refresh_token", creds.refresh_token)
        )

    @Test
    @Order(1)
    fun `Logging as root returns an access_token that is a string`() {
        DoAuth("root", "root")
            .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("access_token").isString)
    }

    @Test
    @Order(2)
    fun `Using wrong credentials returns 400 and shows an error`() {
        DoAuth("root", "wrongpwd")
            .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().`is`(400))
            .andExpect(MockMvcResultMatchers.jsonPath("error_description").isString)
    }

    @Test
    @Order(3)
    fun `Wrong HTTP basic auth returns 401`() {
        mockMvc
            .perform(
                MockMvcRequestBuilders.post("/oauth/token")
                    .param("username", "root").param("password", "wrongpwd").param("grant_type", "password")
                    .with(SecurityMockMvcRequestPostProcessors.httpBasic("cs", "a55557848a884359a980dce828af33b"))
            )
            .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().`is`(401))
    }

    @Test
    @Order(4)
    fun `Test user has role with same name`() {
        val testUserCreds = GetCredentials("testuser", "testpassword")

        DoGet("/private/roles", testUserCreds)
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().`is`(200))
            .andExpect(MockMvcResultMatchers.jsonPath("roles").value("testuser"))
    }

    @Test
    @Order(5)
    fun `Deleting testUser role`() {
        val rootCreds = GetCredentials("root", "root")
        val testUserCreds = GetCredentials("testuser", "testpassword")

        DoGet("/admin/delrole/testuser", rootCreds).andReturn()

        DoGet("/private/roles", testUserCreds)
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().`is`(200))
            .andExpect(MockMvcResultMatchers.jsonPath("roles").isEmpty)
    }
}
