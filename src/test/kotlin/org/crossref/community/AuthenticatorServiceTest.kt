package org.crossref.community

import net.wussmann.kenneth.mockfuel.MockFuelStore
import net.wussmann.kenneth.mockfuel.data.MockResponse
import net.wussmann.kenneth.mockfuel.junit.MockFuelExtension
import org.crossref.community.model.AuthenticationResponse
import org.crossref.community.services.impl.AuthenticatorService
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.test.context.web.WebAppConfiguration

@Extensions(ExtendWith(MockFuelExtension::class))
@WebAppConfiguration
class AuthenticatorServiceTest {

    companion object {
        const val TEST_USERNAME = "foo"
        const val TEST_PASSWORD = "bar"
        const val TEST_ROLE1 = "role1"
        const val TEST_ROLE2 = "role2"
    }

    private val authServiceClient = AuthenticatorService("http://url.com", "asdf", "asdasdf")

    @Test
    fun `Correct login returns the right AuthenticationResponse`(mockFuelStore: MockFuelStore) {
        val expectedBody = """{
                "status": "success",
                "username": "cdelojo@crossref.org"
            }
        """.trimIndent()

        // we enqueue this response to be served when any request comes in
        mockFuelStore.enqueue(
            MockResponse(statusCode = 200, body = expectedBody.toByteArray())
        )

        val resp = authServiceClient.authenticate("cdelojo@crossref.org", "fakepwd")

        Assert.assertEquals(resp.message, "Authenticated")
        Assert.assertEquals(resp.status, AuthenticationResponse.AuthStatus.SUCCESS)
    }

    @Test
    fun `Incorrect login returns the right AuthenticationResponse`(mockFuelStore: MockFuelStore) {
        val expectedBody = """{
                "status": "failure",
                "message": "Invalid username or password"
            }
        """.trimIndent()

        // we enqueue this response to be served when any request comes in
        mockFuelStore.enqueue(
            MockResponse(statusCode = 400, body = expectedBody.toByteArray())
        )

        val resp = authServiceClient.authenticate("cdelojo@crossref.org", "fakepwd")

        Assert.assertEquals(resp.message, "Invalid username or password")
        Assert.assertEquals(resp.status, AuthenticationResponse.AuthStatus.WRONG_CREDENTIALS)
    }

    @Test
    fun `Incorrect HTTP basic authentication`(mockFuelStore: MockFuelStore) {
        val expectedBody = """{
                "status": "failure",
                "message": "Basic HTTP authentication failed"
            }
        """.trimIndent()

        // we enqueue this response to be served when any request comes in
        mockFuelStore.enqueue(
            MockResponse(statusCode = 400, body = expectedBody.toByteArray())
        )

        val resp = authServiceClient.authenticate("cdelojo@crossref.org", "fakepwd")

        Assert.assertEquals(resp.message, "Basic HTTP authentication failed")
        Assert.assertEquals(resp.status, AuthenticationResponse.AuthStatus.SERVICE_ERROR)
    }

    @Test
    fun `Authenticator not accessible`(mockFuelStore: MockFuelStore) {
        val expectedBody = """{}"""

        // we enqueue this response to be served when any request comes in
        mockFuelStore.enqueue(
            MockResponse(statusCode = -1, body = expectedBody.toByteArray())
        )

        val resp = authServiceClient.authenticate("cdelojo@crossref.org", "fakepwd")

        Assert.assertEquals(resp.message, "Internal Error")
        Assert.assertEquals(resp.status, AuthenticationResponse.AuthStatus.SERVICE_ERROR)
    }

    @Test
    fun `Fetching roles`(mockFuelStore: MockFuelStore) {
        val expectedBody = """{
                "status": "success",
                "username": "cdelojo@crossref.org",
                "legacy_roles": [ "role1", "role2" ]
            }""".trimMargin()

        // we enqueue this response to be served when any request comes in
        mockFuelStore.enqueue(
            MockResponse(statusCode = 200, body = expectedBody.toByteArray())
        )

        val resp = authServiceClient.getRoles("cdelojo@crossref.org")

        Assert.assertEquals(resp.map { it.name }, listOf("role1", "role2"))
    }
}
